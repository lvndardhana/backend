import OrderDetail from "../models/OrderDetailModel";
import Orders from "../models/OrderModel"
import { Op } from "sequelize";

export const getProductsByOrderId = async (req, res) => {
  try {
    const orderId = req.params.orderId;
    const page = parseInt(req.query.page) || 1;
    const limit = parseInt(req.query.limit) || 10;
    const currentPage = page - 1;
    const offset = limit * currentPage;
    const countResponse = await OrderDetail.count({
      where: {
        orderId: orderId,
      },
    });

    const totalRows = countResponse;
    const totalPage = Math.ceil(totalRows / limit);

    const response = await OrderDetail.findAll({
      include: [{
        model: Orders,
        attributes: ['bayar', 'kembalian']
      }],
      where: {
        orderId: orderId,
      },
      offset: offset,
      limit: limit,
      order: [['id', 'ASC']]
    });

    res.status(200).json({
      result: response,
      page: page,
      limit: limit,
      totalRows: totalRows,
      totalPage: totalPage
    });
  } catch (error) {
    console.log(error.message);
    res.status(500).json({ error: "Internal server error" });
  }
};

  