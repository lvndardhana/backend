import Product from "../models/ProductModel.js";
import Category from "../models/CategoryModel.js";
import Stock from "../models/StockModel.js";
import Unit from "../models/UnitModel.js";
import { Op, QueryTypes, Sequelize } from "sequelize";
import moment from "moment/moment.js";

export const getProducts = async (req, res) => {
  const page = parseInt(req.query.page) || 1;
  const limit = parseInt(req.query.limit) || 10;
  const search = req.query.search_query || "";
  const currentPage = page - 1;
  const offset = limit * currentPage;
  const totalRows = await Product.count({
    where: {
      [Op.or]: [
        {
          namaProduk: {
            [Op.iLike]: "%" + search + "%",
          },
        },
        {
          kodeProduk: {
            [Op.iLike]: "%" + search + "%",
          },
        },
      ],
    },
  });
  const totalPage = Math.ceil(totalRows / limit);
  const result = await Product.findAll({
    attributes: [
      "id",
      ["kodeProduk", "productKodeProduk"],
      "namaProduk",
      "qty",
      "hargaBeli",
      "hargaJual",
      "categoryId",
      "supplierId",
      "unitId",
    ],
    include: [
      {
        model: Category,
        attributes: ["namaKategori"],
      },
      {
        model: Stock,
        attributes: ["productKodeProduk"],
      },
      {
        model: Unit,
        attributes: ["namaSatuan"],
      },
    ],
    where: {
      [Op.or]: [
        {
          namaProduk: {
            [Op.iLike]: "%" + search + "%",
          },
        },
        {
          kodeProduk: {
            [Op.iLike]: "%" + search + "%",
          },
        },
      ],
    },
    offset: offset,
    limit: limit,
    order: [["kodeProduk", "DESC"]],
  });
  res.json({
    result: result,
    page: Number(page),
    limit: Number(limit),
    totalRows: totalRows,
    totalPage: totalPage,
  });
};

export const getProductsOutOfStock = async (req, res) => {
  try {
    const page = parseInt(req.query.page) || 1;
    const limit = parseInt(req.query.limit) || 10;
    const currentPage = page - 1;
    const offset = limit * currentPage;
    const totalRows = await Product.count({
      where: {
        [Op.or]: [
          {
            qty: {
              [Op.lte]: 5,
            },
          },
        ],
      },
    });
    const totalPage = Math.ceil(totalRows / limit);
    const result = await Product.findAll({
      attributes: ["id", "namaProduk", "qty"],
      where: {
        [Op.or]: [
          {
            qty: {
              [Op.lte]: 5,
            },
          },
        ],
      },
      offset: offset,
      limit: limit,
      order: [["id", "DESC"]],
    });
    res.json({
      result: result,
      page: Number(page),
      limit: Number(limit),
      totalRows: totalRows,
      totalPage: totalPage,
    });
  } catch (error) {
    console.log(error);
  }
};

export const countProductOutOfStock = async (req, res) => {
  try {
    const response = await Product.count({
      where: {
        [Op.or]: [
          {
            qty: {
              [Op.lte]: 5,
            },
          },
        ],
      },
    });

    res.json(
      response
    );
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

export const countProduct = async (req, res) => {
  try {
    const response = await Product.count({});
    res.status(200).json(response);
  } catch (error) {
    console.log(error.message);
    res.status(500).json({ msg: error.message });
  }
};

export const getProductById = async (req, res) => {
  try {
    const response = await Product.findOne({
      attributes: [["kodeProduk", "productKodeProduk"], "namaProduk"],
      where: {
        kodeProduk: req.params.productKodeProduk,
      },
    });

    if (!response) {
      return res
        .status(404)
        .json({
          msg: `Product with kodeProduk ${req.params.productKodeProduk} not found`,
        });
    }

    res.status(200).json(response);
  } catch (error) {
    console.log(error.message);
    res.status(500).json({ msg: error.message });
  }
};

export const getProductsById = async (req, res) => {
  try {
    const response = await Product.findOne({
      where: {
        id: req.params.id,
      },
    });
    res.status(200).json(response);
  } catch (error) {
    console.log(error.message);
  }
};

export const createProduct = async (req, res) => {
  try {
    await Product.create(req.body);
    res.status(201).json({ msg: "Product created" });
  } catch (error) {
    console.log(error.message);
  }
};

export const updateProduct = async (req, res) => {
  try {
    await Product.update(req.body, {
      where: {
        id: req.params.id,
      },
    });
    res.status(200).json({ msg: "Product updated" });
  } catch (error) {
    console.log(error.message);
  }
};

export const deleteProduct = async (req, res) => {
  try {
    await Product.destroy({
      where: {
        id: req.params.id,
      },
    });
    res.status(200).json({ msg: "Product deleted" });
  } catch (error) {
    console.log(error.message);
  }
};
