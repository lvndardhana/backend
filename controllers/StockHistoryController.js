import StockHistory from "../models/StockHistoryModel.js";
import Product from "../models/ProductModel.js";
import Supplier from "../models/SupplierModel.js";
import Unit from "../models/UnitModel";
import { Op } from "sequelize";

export const getStocks = async (req, res) => {
  const page = parseInt(req.query.page) || 1;
  const limit = parseInt(req.query.limit) || 10;
  const currentPage = page - 1;
  const offset = limit * currentPage;
  const search = req.query.search_query || "";
  const totalRows = await StockHistory.count({
    include: [
      {
        model: Product,
        attributes: ["namaProduk"],
        where: {
          [Op.or]: [
            {
              namaProduk: {
                [Op.iLike]: "%" + search + "%",
              },
            },
          ],
        },
      },
    ],
  });
  const totalPage = Math.ceil(totalRows / limit);
  const result = await StockHistory.findAll({
    attributes: [
      "invoice",
      "id",
      "productKodeProduk",
      "expired",
      "qtyIn",
      "tglMasuk",
      "supplierId",
    ],
    include: [
      {
        model: Product,
        attributes: ["namaProduk"],
        where: {
          [Op.or]: [
            {
              namaProduk: {
                [Op.iLike]: "%" + search + "%",
              },
            },
          ],
        },
      },
      {
        model: Supplier,
        attributes: ["namaSupplier"],
      },
      { model: Unit, attributes: ["namaSatuan"] },
    ],

    offset: offset,
    limit: limit,
    order: [["id", "DESC"]],
  });
  res.json({
    result: result,
    page: Number(page),
    limit: Number(limit),
    totalRows: totalRows,
    totalPage: totalPage,
  });
};
