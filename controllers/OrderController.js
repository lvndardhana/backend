import db from "../config/Database.js";
import moment from "moment/moment.js";
import Order from "../models/OrderModel.js";
import OrderDetail from "../models/OrderDetailModel.js";
import Stock from "../models/StockModel.js";
import Product from "../models/ProductModel.js";
import { Op, QueryTypes, Sequelize } from "sequelize";

/**
 * API generate Invoice Number
 * @param {*} trx
 * @returns
 */
export const generateInvoiceNumber = async (req, res) => {
  const trx = await db.transaction();
  try {
    let timeNow = moment().format("YYMMDD");
    let tmpInvNo = await Order.findOne({
      order: [["invoiceNo", "DESC"]],
      limit: 1,
      transaction: trx,
    });
    tmpInvNo = !tmpInvNo ? `${timeNow}00000` : tmpInvNo.invoiceNo;
    let ctxTime = tmpInvNo.slice(7, 9);
    let num = parseInt(tmpInvNo.slice(9));
    let chkTimeNow = timeNow.slice(4, 6);

    let invoiceNo = "";

    if (chkTimeNow !== ctxTime) {
      num = 1;
      invoiceNo = `INV${timeNow}${num.toString().padStart(5, "0")}`;
    } else {
      num += 1;
      invoiceNo = `INV${timeNow}${num.toString().padStart(5, "0")}`;
    }

    await trx.commit();

    res.status(200).json({ data: invoiceNo });
  } catch (error) {
    await trx.rollback();
    console.log(error.message);
    res.status(500).json({ msg: error.message });
  }
};

/**
 * API create order
 * @param {*} trx
 * @returns
 */
export const createOrder = async (req, res) => {
  const trx = await db.transaction();
  try {
    /**
     * checking data order by invoice_no
     */
    let invNo = req.body.invoiceNo;
    let chkOrder = await Order.findOne({ where: { invoiceNo: invNo } });
    if (chkOrder) {
      return res.status(400).json({ msg: `Order ${invNo} sudah ada!` });
    }

    let dataOrder = await Order.create(
      {
        invoiceNo: invNo,
        username: req.body.username,
      },
      { transaction: trx }
    );

    let subtotal = 0;
    for (let i = 0; i < req.body.items.length; i++) {
      const el = req.body.items[i];

      let chkProduk = await Product.findOne({
        where: { kodeProduk: el.kdProduk },
        transaction: trx,
      });
      if (!chkProduk) {
        return res
          .status(400)
          .json({ msg: `Produk ${el.kdProduk} tidak ditemukan!` });
      }
      if (chkProduk.qty < el.qty) {
        return res
          .status(400)
          .json({ msg: `Qty Produk ${el.kdProduk} tidak mencukupi!` });
      }

      let checkProduk = await Stock.findAll({
        where: { productKodeProduk: el.kdProduk },
        order: [['tglMasuk', 'ASC']],
        transaction: trx,
      });
      if (!checkProduk || checkProduk.length === 0) {
        return res
          .status(400)
          .json({ msg: `Produk ${el.kdProduk} tidak ditemukan!` });
      }

      let remainingQty = el.qty;

      for (let j = 0; j < checkProduk.length; j++) {
        const stock = checkProduk[j];

        if (remainingQty <= stock.qtyIn) {
          await Stock.update(
            {
              qtyIn: stock.qtyIn - remainingQty,
              tglMasuk: stock.tglMasuk, // Use the invoice date of the current stock item
            },
            {
              where: {
                id: stock.id,
              },
              transaction: trx,
            }
          );

          remainingQty = 0;
          break;
        } else {
          remainingQty -= stock.qtyIn;

          await Stock.update(
            {
              qtyIn: 0,
            },
            {
              where: {
                id: stock.id,
              },
              transaction: trx,
            }
          );
        }
      }

      if (remainingQty > 0) {
        return res
          .status(400)
          .json({ msg: `Qty Produk ${el.kdProduk} tidak mencukupi!` });
      }

      subtotal += parseFloat(el.harga) * Math.floor(el.qty);

      await OrderDetail.create(
        {
          kdProduk: el.kdProduk,
          orderId: dataOrder.id,
          qty: el.qty,
          harga: parseFloat(el.harga),
          hargaBeli: parseFloat(chkProduk.hargaBeli),
          namaProduk: chkProduk.namaProduk,
          subtotal: parseFloat(el.harga) * Math.floor(el.qty)
        },
        { transaction: trx }
      );

      await Product.update(
        { qty: chkProduk["qty"] - el.qty },
        { where: { kodeProduk: el.kdProduk }, transaction: trx }
      );
    }

    const taxRate = (req.body.tax * subtotal) / 100;
    const discountRate = (req.body.discount * subtotal) / 100;
    const total = subtotal - discountRate + taxRate;
    const kembalian = req.body.bayar - total;

    await Order.update(
      {
        tax: req.body.tax,
        taxRate: taxRate,
        discount: req.body.discount,
        discountRate: discountRate,
        subtotal: subtotal,
        bayar: req.body.bayar,
        kembalian: kembalian,
        total: total,
      },
      { where: { id: dataOrder.id }, transaction: trx }
    );

    await trx.commit();

    return res.json({ msg: `Order ${invNo} berhasil dibuat!` });
  } catch (error) {
    await trx.rollback();
    console.log(error);
    return res.status(500).json({ msg: "Terjadi kesalahan pada server!" });
  }
};

/**
 * API generate Invoice Number
 * @param {*} trx
 * @returns
 */

export const productList= async(req, res) =>{
  try {
    // let kdProduk = req.body.kdProduk || ''
    // let namaProduk = req.body.namaProduk || ''
    let data = await Product.findAll({
      // where: {
      //   expired: {
      //     [Op.gte]: new Date()
      //   }
        // [Op.or]: [
        //   {
        //     kodeProduk: {
        //       [Op.iLike]: '%'+kdProduk+'%'
        //     },
        //   }, {
        //     namaProduk: {
        //       [Op.iLike]: '%'+namaProduk+'%'
        //     }
        //   }
        // ]
      // },
      attributes: [['kodeProduk', 'kdProduk'], 'namaProduk', 'qty', ['hargaJual','harga']]
    })
      
    res.status(200).json({data: data});
  } catch (error) {
      console.log(error.message);
      res.status(500).json({msg: error.message});
  }
}

export const countOrderListToday = async (req, res) => {
  try {
    const {
      startDate = moment().startOf("day").format("YYYY-MM-DD, h:mm:ss a"),
      endDate = moment().endOf("day").format("YYYY-MM-DD, h:mm:ss a")
    } = req.query;

      const response = await Order.count({
      where: {
        createdAt: {
          [Op.between]: [startDate, endDate],
        },
      },
    });


    res.status(200).json(response);
  } catch (error) {
    console.log(error.message);
    res.status(500).json({ msg: error.message });
  }
};

export const orderListToday = async (req, res) => {
  try {
    const {
      startDate = moment().startOf("day").format("YYYY-MM-DD, h:mm:ss a"),
      endDate = moment().endOf("day").format("YYYY-MM-DD, h:mm:ss a"),
      page = 1,
      limit = 10,
    } = req.query;

    const currentPage = page - 1;

    const offset = limit * currentPage;

    const totalRows = await Order.count({
      where: {
        createdAt: {
          [Op.between]: [startDate, endDate],
        },
      },
    });

    const totalPage = Math.ceil(totalRows / limit);

    const result = await db.query(
      `
      SELECT
      row_number() over (order by ord.id desc) AS no, ord."id" AS orderId,ord."invoiceNo", ord.username, ord.total,
      ord."createdAt" AS tgl_transaksi, array_agg(prd."namaProduk") AS produkList
      FROM orders AS ord
      LEFT JOIN "orderDetails" AS ords ON ords."orderId" = ord.id
      LEFT JOIN product AS prd ON prd."kodeProduk" = ords."kdProduk"
      WHERE ord."createdAt" between :startDate and :endDate
      GROUP BY ord.id
      ORDER BY ord.id desc
      LIMIT :limit
      OFFSET :offset
    `,
      {
        replacements: {
          startDate: startDate,
          endDate: endDate,
          limit: limit,
          offset: offset,
        },
        type: QueryTypes.SELECT,
      }
    );

    res.status(200).json({
      result: result,
      page: Number(page),
      limit: Number(limit),
      totalRows: totalRows,
      totalPage: totalPage,
    });
  } catch (error) {
    console.log(error.message);
    res.status(500).json({ msg: error.message });
  }
};


export const orderList = async (req, res) => {
  try {
    const {
      startDate = moment().startOf("month").format("YYYY-MM-DD, h:mm:ss a"),
      endDate = moment().endOf("month").format("YYYY-MM-DD, h:mm:ss a"),
      page = 1,
      limit = 10,
    } = req.query;

    const currentPage = page - 1;

    const offset = limit * currentPage;

    const totalRows = await Order.count({
      where: {
        createdAt: {
          [Op.between]: [startDate, endDate],
        },
      },
    });

    const totalPage = Math.ceil(totalRows / limit);

    const result = await db.query(
      `
      SELECT
      row_number() over (order by ord.id desc) AS no, ord."id" AS orderId,ord."invoiceNo", ord.username, ord.total,
      ord."createdAt" AS tgl_transaksi, array_agg(prd."namaProduk") AS produkList
      FROM orders AS ord
      LEFT JOIN "orderDetails" AS ords ON ords."orderId" = ord.id
      LEFT JOIN product AS prd ON prd."kodeProduk" = ords."kdProduk"
      WHERE ord."createdAt" between :startDate and :endDate
      GROUP BY ord.id
      ORDER BY ord.id desc
      LIMIT :limit
      OFFSET :offset
    `,
      {
        replacements: {
          startDate: startDate,
          endDate: endDate,
          limit: limit,
          offset: offset,
        },
        type: QueryTypes.SELECT,
      }
    );

    res.status(200).json({
      result: result,
      page: Number(page),
      limit: Number(limit),
      totalRows: totalRows,
      totalPage: totalPage,
    });
  } catch (error) {
    console.log(error.message);
    res.status(500).json({ msg: error.message });
  }
};

export const orderListById = async (req, res) => {
  try {
    const { orderId } = req.params;

    const result = await db.query(
      `
      SELECT
      row_number() over (order by ord.id desc) AS no, ord."id" AS orderId, ord.total, ord.bayar, ord.kembalian, array_agg(prd."namaProduk") AS produkList
      FROM orders AS ord
      LEFT JOIN "orderDetails" AS ords ON ords."orderId" = ord.id
      LEFT JOIN product AS prd ON prd."kodeProduk" = ords."kdProduk"
      WHERE ord.id = :orderId
      GROUP BY ord.id
      ORDER BY ord.id desc
    `,
      {
        replacements: { orderId: orderId },
        type: QueryTypes.SELECT,
      }
    );

    if (result.length === 0) {
      return res.status(404).json({ msg: `Order with ID ${orderId} not found` });
    }

    res.status(200).json({
      result: result[0],
    });
  } catch (error) {
    console.log(error.message);
    res.status(500).json({ msg: error.message });
  }
};


export const getOmzet = async (req, res) => {
  try {
    const {
      startDate = moment().startOf("month").format("YYYY-MM-DD, h:mm:ss a"),
      endDate = moment().endOf("month").format("YYYY-MM-DD, h:mm:ss a"),
    } = req.query;

    // let date =
    //   req.body.search_date && req.body.search_date.length > 0
    //     ? req.body.search_date
    //     : [
    //         moment().startOf("month").format("YYYY-MM-DD"),
    //         moment().endOf("month").format("YYYY-MM-DD"),
    //       ];

    const result = await Order.findAll({
      attributes: [[db.literal("sum(total)"), "omzet"]],
      where: {
        createdAt: {
          [Op.between]: [startDate, endDate],
        },
      },
    });

    let data = result[0];

    res.status(200).json({
      data,
    });
  } catch (error) {
    console.log(error.message);
    res.status(500).json({ msg: error.message });
  }
};

export const getOmzetDaily = async (req, res) => {
  try {
    const {
      startDate = moment().startOf("day").format("YYYY-MM-DD, h:mm:ss a"),
      endDate = moment().endOf("day").format("YYYY-MM-DD, h:mm:ss a"),
    } = req.query;

    // let date =
    //   req.body.search_date && req.body.search_date.length > 0
    //     ? req.body.search_date
    //     : [
    //         moment().startOf("month").format("YYYY-MM-DD"),
    //         moment().endOf("month").format("YYYY-MM-DD"),
    //       ];

    const result = await Order.findAll({
      attributes: [[db.literal("sum(total)"), "omzet"]],
      where: {
        createdAt: {
          [Op.between]: [startDate, endDate],
        },
      },
    });

    let data = result[0];

    res.status(200).json({
      data,
    });
  } catch (error) {
    console.log(error.message);
    res.status(500).json({ msg: error.message });
  }
};


export const getProfit = async (req, res) => {
  try {
    // let date =
    //   req.body.search_date && req.body.search_date.length > 0
    //     ? req.body.search_date
    //     : [
    //         moment().startOf("month").format("YYYY-MM-DD"),
    //         moment().endOf("month").format("YYYY-MM-DD"),
    //       ];

    const {
      startDate = moment().startOf("month").format("YYYY-MM-DD, h:mm:ss a"),
      endDate = moment().endOf("month").format("YYYY-MM-DD, h:mm:ss a"),
    } = req.query;

    const result = await db.query(
      `
      SELECT SUM((ords.harga - ords."hargaBeli")*qty) AS profit
      FROM orders AS ord
      LEFT JOIN "orderDetails" AS ords ON ords."orderId" = ord.id
      WHERE ord."createdAt" between :startDate and :endDate
    `,
      {
        replacements: {
          startDate: startDate,
          endDate: endDate,
        },
        type: QueryTypes.SELECT,
      }
    );
    let data = result[0];
    res.status(200).json({
      data,
    });
  } catch (error) {
    console.log(error.message);
    res.status(500).json({ msg: error.message });
  }
};

export const getPendapatanPerbulan = async (req, res) => {
  const startDate = moment().startOf("year").format("YYYY-MM-DD, h:mm:ss a");
  const endDate = moment().endOf("year").format("YYYY-MM-DD, h:mm:ss a");

  try {
    const dataTransaksi = await db.query(
      `
      SELECT
        row_number() over (order by ord.id desc) AS no,
        ord."invoiceNo",
        ord.username,
        ord.total,
        ord."createdAt" AS tgl_transaksi,
        array_agg(prd."namaProduk") AS produkList
      FROM orders AS ord
      LEFT JOIN "orderDetails" AS ords ON ords."orderId" = ord.id
      LEFT JOIN product AS prd ON prd."kodeProduk" = ords."kdProduk"
      WHERE ord."createdAt" BETWEEN :startDate AND :endDate
      GROUP BY ord.id
      ORDER BY ord.id ASC
      `,
      {
        replacements: {
          startDate: startDate,
          endDate: endDate,
        },
        type: QueryTypes.SELECT,
      }
    );

    const pendapatanPerBulan = {};
    dataTransaksi.forEach(transaksi => {
      const tglTransaksi = new Date(transaksi.tgl_transaksi);
      const bulan = tglTransaksi.toLocaleString('default', { month: 'long' });
      const total = Number(transaksi.total);

      if (!pendapatanPerBulan[bulan]) {
        pendapatanPerBulan[bulan] = total;
      } else {
        pendapatanPerBulan[bulan] += total;
      }
    });

    res.json(pendapatanPerBulan);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Gagal mengambil data pendapatan' });
  }
};