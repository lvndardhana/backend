import {Sequelize} from "sequelize";
import db from "../config/Database.js";

const {DataTypes} = Sequelize;

const Stock = db.define('stock',{
    invoice:{
        type:DataTypes.STRING
    },
    tglMasuk: {
        type: DataTypes.DATEONLY
    }, 
    qtyIn: {
        type: DataTypes.INTEGER
    }
    ,expired: {
        type: DataTypes.DATEONLY
    }
},{
    freezeTableName:true
});

export default Stock;