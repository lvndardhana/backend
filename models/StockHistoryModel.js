import {Sequelize} from "sequelize";
import db from "../config/Database.js";

const {DataTypes} = Sequelize;

const StockHistory = db.define('stockHistory',{
    invoice:{
        type:DataTypes.STRING
    },
    tglMasuk: {
        type: DataTypes.DATEONLY
    }, 
    qtyIn: {
        type: DataTypes.INTEGER
    }
    , expired: {
        type: DataTypes.DATEONLY
    },supplierId: {
        type: DataTypes.INTEGER // Adjust the data type as per your schema
      },
      unitId: {
        type: DataTypes.INTEGER // Adjust the data type as per your schema
      },
      kodeProduk: {
        type: DataTypes.STRING
      }
},{
    freezeTableName:true
});

export default StockHistory;