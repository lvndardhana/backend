import express from "express";
import {
    getProducts,
    getProductsOutOfStock,
    countProduct,
    countProductOutOfStock,
    getProductsById,
    getProductById,
    createProduct,
    updateProduct,
    deleteProduct
} from "../controllers/ProductController.js";

const router = express.Router();

router.get('/products', getProducts);
router.get('/products/count', countProduct);
router.get('/products/outofstock', getProductsOutOfStock);
router.get('/products/outofstock/count', countProductOutOfStock);
router.get('/products/:id', getProductsById);
router.get('/product/name/:productKodeProduk', getProductById);
router.post('/products', createProduct);
router.patch('/products/edit/:id', updateProduct);
router.delete('/products/:id', deleteProduct);

export default router;