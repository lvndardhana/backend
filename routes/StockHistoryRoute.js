import express from "express";
import {
    getStocks
} from "../controllers/StockHistoryController.js";

const router = express.Router();

router.get('/stocks-history/list', getStocks);

export default router;