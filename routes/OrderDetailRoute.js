import express from "express";
import { getProductsByOrderId } from "../controllers/OrderDetailController";

const router = express.Router();

router.get('/order-detail/:orderId', getProductsByOrderId);

export default router;
