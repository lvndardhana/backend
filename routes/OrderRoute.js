import express from "express";
import {
    generateInvoiceNumber,
    orderListById,
    createOrder,
    productList,
    orderList,
    orderListToday,
    countOrderListToday,
    getOmzet,
    getOmzetDaily,
    getProfit,
    getPendapatanPerbulan
} from "../controllers/OrderController.js";

const router = express.Router();

router.get('/order/invoice-number', generateInvoiceNumber);
router.post('/order/create', createOrder);
router.get('/order/product-list', productList);
router.get('/order-list/:orderId', orderListById);
router.get('/order/list/today', orderListToday)
router.get('/order/count/today', countOrderListToday)
router.get('/order/list', orderList)
router.get('/order/omzet', getOmzet);
router.get('/order/omzet/daily', getOmzetDaily);
router.get('/order/profit', getProfit);
router.get('/order/pendapatan-perbulan', getPendapatanPerbulan);
// router.post('/order/list', orderList);
// router.post('/order/omzet', getOmzet);
// router.post('/order/profit', getProfit);

export default router;
