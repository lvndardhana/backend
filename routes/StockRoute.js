import express from "express";
import {
    getStockById,
    createStock,
    getExpiredStocks,
    getCountExpiredStocks,
    deleteStock
} from "../controllers/StockController.js";

const router = express.Router();

router.get('/stock-detail/:productKodeProduk', getStockById);
router.post('/stocks', createStock);
router.get('/stocks/expired', getExpiredStocks);
router.get('/stocks/expired/count', getCountExpiredStocks);
router.delete('/stocks/:id', deleteStock);

export default router;
